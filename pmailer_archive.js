(function (jQuery) {

    Drupal.behaviors.pmailer_archive = {
        attach: function(context) {
            
            var directory_counter = Drupal.settings.js_vars.list_count;
                    
            var reload = function(data)
            {     
                jQuery('#arc').html(data)
                
                /*
                 * forward button
                 */
                
                //if on last page disable forward button
                if (parseInt(jQuery('#pager_first').val()) == parseInt(jQuery('#pager_last').val())){
                    jQuery('#page_forward').attr('disabled','disabled');                    
                } else{
                    jQuery('#page_forward').attr('disabled','');     
                }
                
                /*
                 * back button
                 */
                                
                // Disabled back button when pager is at 1
                if (parseInt(jQuery('#pager_first').val()) == 1){
                    jQuery('#page_back').attr('disabled','disabled');                    
                } else{
                    jQuery('#page_back').attr('disabled','');     
                }
                
            }
                
            var start = 1;           
            
            if (parseInt(jQuery('#pager_total').val()) <= directory_counter){
                jQuery('#page_forward').attr('disabled','disabled');
            }
           
            
            jQuery('#page_forward').bind('click', function(){
                              
                current_Page = parseInt(jQuery('#pager_first').val()) + 1;
                jQuery('#page_info').html(' Page '+current_Page+' / ' +jQuery('#pager_last').val() + ' ');
                
               
                jQuery('#page_forward').attr('disabled','disabled');     
                jQuery('#page_back').attr('disabled','disabled'); 
               
                start = parseInt(jQuery('#pager_first').val()) + 1;
               
                jQuery('#arc').html("<div align='center'><img src='"+Drupal.settings.basePath+"/sites/all/modules/pmailer/images/ajax-loader.gif' /></div>");
                jQuery.get('load/'+start,  null, reload);
                
                // preventing entire page from reloading
                false;
            });
            
            jQuery('#page_back').bind('click', function(){
              
                current_Page = parseInt(jQuery('#pager_first').val()) - 1;
                jQuery('#page_info').html(' Page '+current_Page+' / ' +jQuery('#pager_last').val() + ' ');
              
                jQuery('#page_forward').attr('disabled','disabled');     
                jQuery('#page_back').attr('disabled','disabled'); 
                                 
                start = parseInt(jQuery('#pager_first').val()) - 1;  
                
                jQuery('#arc').html("<div align='center'><img src='"+Drupal.settings.basePath+"/sites/all/modules/pmailer/images/ajax-loader.gif' /></div>");
                jQuery.get('load/'+start, null, reload);
                // preventing entire page from reloading
                false;
            });    
         
        }
    };    
}(jQuery));

