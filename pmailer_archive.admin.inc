<?php

/*
 * @file
 * pMailer Directory Module Admin Settings
 *
 * implementation of hook_admin_settings
 * @return <type>
 */

function pmailer_archive_admin_settings(){

  $form['pmailer_archive_account_info'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => TRUE,
    '#title' => 'Insert pMailer API Information',
  );

  $form['pmailer_archive_account_info']['pmailer_archive_server'] = array(
    '#type' => 'textfield',
    '#title' => t('pMailer server'),
    '#required' => TRUE,
    '#default_value' => variable_get('pmailer_archive_server', 'qa.pmailer.net'),
    '#description' => t('Specify pmailer server - Excluding the protocol (remove http:// or https:// from the url).'),
  );

  $form['pmailer_archive_account_info']['pmailer_archive_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('pMailer API key'),
    '#required' => TRUE,
    '#default_value' => variable_get('pmailer_archive_api_key', ''),
    '#description' => t('The API key for your pmailer account. '),
  );

  $api_key = variable_get('pmailer_archive_api_key', FALSE);
  $server = variable_get('pmailer_archive_server', FALSE);


    $form['result_counter'] = array(
      '#type' => 'select',
      '#title' => t('Specify the number of items in list.'),
      '#default_value' => variable_get('result_counter',5),
      '#options' => array(
        '5 items'=>5,
        '10 items' => 10,   
        '20 items' => 20),
      '#description' => t('Setting to specify the number of previous entries to display on each page of the pMailer directory listing.'),
    );
 
  $form['#validate'][] = 'pmailer_archive_admin_settings_validate';
  return system_settings_form($form);
}

/**
 * validate the admin settings 
 *
 * @param <type> $form
 * @param <type> $form_state
 */
function pmailer_archive_admin_settings_validate($form, &$form_state){
  $server = $form['pmailer_archive_account_info']['pmailer_archive_server']['#value'];
  if(substr($server, 0, 7) == 'http://' || substr($server, 0, 8) == 'https://'){
    form_set_error('title', t('URL is invalid. Please remove http:// in the beginning of your URL.'));
  }
}